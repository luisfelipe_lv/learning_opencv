# Contour Features
# Contour approximation
import cv2
import numpy as np 

img = cv2.imread('photos/contourTest.jpg',1)
img2 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(img2,127,255,0)
contours,hierarchy = cv2.findContours(thresh,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)

cnt = contours[0]
epsilon1 = 0.1*cv2.arcLength(cnt,True)
epsilon2 = 0.01*cv2.arcLength(cnt,True)
approx1 = cv2.approxPolyDP(cnt,epsilon1,True)
approx2 = cv2.approxPolyDP(cnt,epsilon2,True)
approx_show1 = cv2.drawContours(img.copy(),[approx1],0,(0,255,0),3)
approx_show2 = cv2.drawContours(img.copy(),[approx2],0,(0,255,0),3)

cv2.namedWindow('approx1', cv2.WINDOW_NORMAL), cv2.namedWindow('approx2', cv2.WINDOW_NORMAL),cv2.namedWindow('original',cv2.WINDOW_NORMAL)
cv2.imshow('original',img), cv2.imshow('approx1',approx_show1), cv2.imshow('approx2',approx_show2)
cv2.waitKey(0) & 0xFF
cv2.destroyAllWindows()