# Contour Features
# Fitting a Line
import cv2
import numpy as np 

img = cv2.imread('photos/thunder.png',1)
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
_,thresh = cv2.threshold(gray,240,255,0)
contours,_ = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
cnt = contours[0]

rows,cols = img.shape[:2]
[vx,vy,x,y] = cv2.fitLine(cnt,cv2.DIST_L2,0,0.01,0.01)
lefty = int((-x*vy/vx) + y)
righty = int(((cols-x)*vy/vx)+y)
out = cv2.line(img,(cols-1,righty),(0,lefty),(0,255,0),2)

cv2.namedWindow('line',cv2.WINDOW_NORMAL)
cv2.imshow('line',out)
cv2.waitKey(0) & 0xFF
cv2.destroyAllWindows()