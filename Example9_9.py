# Contour Properties

import cv2
import numpy as np 

img = cv2.imread('photos/thunder.png',1)
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
_,thresh = cv2.threshold(gray,240,255,0)
contours,_ = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
cnt = contours[0]

# Computing Aspect Ratio
x,y,w,h = cv2.boundingRect(cnt)
aspect_ratio = float(w)/h

# Computing Extent
area = cv2.contourArea(cnt)
x,y,w,h = cv2.boundingRect(cnt)
rect_area = w*h
extent = float(area)/rect_area

# Computing Solidity
area = cv2.contourArea(cnt)
hull = cv2.convexHull(cnt)
hull_area = cv2.contourArea(hull)
solidity = float(area)/hull_area

# Computing Equivalent Diameter
area = cv2.contourArea(cnt)
equi_diameter = np.sqrt(4*area/np.pi)

# Computing orientation
(x,y), (MA,ma), angle = cv2.fitEllipse(cnt)

# Mask and pixel points
mask = np.zeros(gray.shape,np.uint8)
cv2.drawContours(mask,[cnt],0,255,-1)
pixelpoints = np.transpose(np.nonzero(mask))
#pixelpoints = cv2.findNonZero(mask)

# Maximum and minimum values and their locations
min_val,max_val,min_loc,max_loc = cv2.minMaxLoc(gray,mask=mask)

# Mean color or Mean Intensity
mean_val = cv2.mean(img, mask=mask)

# Extreme points
leftmost = tuple(cnt[cnt[:,:,0].argmin()][0])
rightmost = tuple(cnt[cnt[:,:,0].argmax()][0])
topmost = tuple(cnt[cnt[:,:,1].argmin()][0])
bottommost = tuple(cnt[cnt[:,:,1].argmax()][0])

print("Aspect Ratio =",aspect_ratio)
print("Extent =",extent)
print("Solidity =",solidity)
print("Equivalent Diameter =",equi_diameter)
print("Orientation =",angle)
print("(min_val,max_val,min_loc,max_loc) = ",min_val,max_val,min_loc,max_loc)
print("Mean Color =",mean_val)
print("Leftmost, rightmost, topmost, bottommost =",leftmost, rightmost, topmost, bottommost)