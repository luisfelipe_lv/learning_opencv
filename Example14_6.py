# Edge detection
# ORB algorithm
import numpy as np
import cv2
from matplotlib import pyplot as plt

def draw_keypoints(vis, keypoints, color = (0, 255, 255)):
    for kp in keypoints:
            x, y = kp.pt
            cv2.circle(vis, (int(x), int(y)), 2, color)

img = cv2.imread('photos/simple.jpg')

# Initiate STAR detector
#orb = cv2.ORB()
#orb_c = orb.create()
orb_c = cv2.ORB_create()

# find the keypoints with ORB
kp = orb_c.detect(img, None)

# Compute the descriptors with ORB
kp, des = orb_c.compute(img, kp)

# draw only keypoints location, not size and orientation
draw_keypoints(img,kp,color=(0,255,0))
plt.imshow( cv2.cvtColor(img,cv2.COLOR_BGR2RGB) ),plt.show()

