# Contour Features
# Moments
import cv2
import numpy as np 

img2 = cv2.imread('photos/white_star.png',1)
img = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(img,127,255,0)
contours,hierarchy = cv2.findContours(thresh,1,2)
print('contours = ', contours)
print('contours[0] = ',contours[0])

cnt = contours[0]
M = cv2.moments(cnt)
print(M)

cx = int(M['m10']/M['m00'])
cy = int(M['m01']/M['m00'])
print('Centroid (x,y) = ',cx,cy)
area = cv2.contourArea(cnt)
perimeter = cv2.arcLength(cnt,True)

show_cont = cv2.drawContours(img2,contours,0,(0,255,0),3)
cv2.namedWindow('img_out',cv2.WINDOW_NORMAL)
cv2.imshow('img_out' ,show_cont)
while(cv2.waitKey(0) & 0xFF != ord('q')):
	pass
cv2.destroyAllWindows()
