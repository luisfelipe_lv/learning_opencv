# Histograms
# Histogram calculation and plotting
import cv2
import numpy as np 
from matplotlib import pyplot as plt 

img = cv2.imread('photos/home.jpg',0)

# Computed with OpenCV function
hist1 = cv2.calcHist([img],[0],None,[256],[0,255])

# Computed with Numpy function. Around 40X slower than openCV function!!!
hist2,bins = np.histogram(img.ravel(),256,[0,225])

# For plotting the histogram we can use Numpy without even have to compute the histogram beforehand
plt.subplot(121), plt.imshow(img,cmap='gray'), plt.subplot(122), plt.hist(img.ravel(),256,[0,225])
plt.show()