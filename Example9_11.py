# Contour Properties
# Match Shapes
import cv2
import numpy as np 
from matplotlib import pyplot as plt

img1 = cv2.imread('photos/star.jpg',0)
img2 = cv2.imread('photos/star2.jpeg',0)
img3 = cv2.imread('photos/white_star.png',0)
img4 = cv2.imread('photos/rectangle.png',0)

_,thresh1 = cv2.threshold(img1,127,255,0)
_,thresh2 = cv2.threshold(img2,127,255,0)
_,thresh3 = cv2.threshold(img3,127,255,0)
_,thresh4 = cv2.threshold(img4,127,255,0)

contours,_ = cv2.findContours(thresh1,2,1)
cnt1 = contours[0]

contours,_ = cv2.findContours(thresh2,2,1)
cnt2 = contours[0]

contours,_ = cv2.findContours(thresh3,2,1)
cnt3 = contours[0]

contours,_ = cv2.findContours(thresh4,2,1)
cnt4 = contours[0]

cmp1 = cv2.matchShapes(cnt1,cnt1,1,0.0)
cmp2 = cv2.matchShapes(cnt1,cnt2,1,0.0)
cmp3 = cv2.matchShapes(cnt1,cnt3,1,0.0)
cmp4 = cv2.matchShapes(cnt1,cnt4,1,0.0)

print("Matching Image 1 with itself =",cmp1)
print("Matching Image 1 with Image 2=",cmp2)
print("Matching Image 1 with Image 3=",cmp3)
print("Matching Image 1 with Image 4=",cmp4)

plt.subplot(221),plt.imshow(cv2.cvtColor(img1,cv2.COLOR_BGR2RGB))
plt.subplot(222),plt.imshow(cv2.cvtColor(img2,cv2.COLOR_BGR2RGB))
plt.subplot(223),plt.imshow(cv2.cvtColor(img3,cv2.COLOR_BGR2RGB))
plt.subplot(224),plt.imshow(cv2.cvtColor(img4,cv2.COLOR_BGR2RGB))
plt.show()
