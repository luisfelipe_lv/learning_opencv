# Histograms
# CLAHE equalization
import cv2
import numpy as np 
from matplotlib import pyplot as plt 

img = cv2.imread('photos/tsukuba_1.webp',0)

# Create a CLAHE object (Arguments are optional)
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
cl1 = clahe.apply(img)

# Get histograms img and cl1
hist_img,_ = np.histogram(img.flatten(),256,[0,255])
hist_cl1,_ = np.histogram(cl1.flatten(),256,[0,255])

# Get cumulative histograms for img and cl1
cdf_im = hist_img.cumsum()
cdf_cl = hist_cl1.cumsum()

# Get normalized cumulative histograms for img and cl1
cdf_im_normalized = cdf_im*hist_img.max() / cdf_im.max()
cdf_cl_normalized = cdf_cl*hist_cl1.max() / cdf_cl.max()

# Plot images
plt.subplot(221),plt.imshow(img,cmap='gray')
plt.subplot(222),plt.imshow(cl1,cmap='gray')
plt.subplot(223),plt.plot(cdf_im_normalized,color='b'),plt.hist(img.flatten(),256,[0,255],color='r')
plt.xlim([0,255]),plt.legend(('cdf','histogram'),loc='upper left')
plt.subplot(224),plt.plot(cdf_cl_normalized,color='b'),plt.hist(cl1.flatten(),256,[0,255],color='r')
plt.xlim([0,255]),plt.legend(('cdf','histogram'),loc='upper left')
plt.show()