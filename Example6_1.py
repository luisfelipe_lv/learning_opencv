# Geometric Transformations of Images
# Scaling
import cv2
import numpy as np 

img = cv2.imread('photos/messi.jpg')

res1 = cv2.resize(img,None,fx=2,fy=2,interpolation=cv2.INTER_CUBIC)

# OR

height,width = img.shape[:2]
res2 = cv2.resize(img,(2*width,2*height),interpolation=cv2.INTER_CUBIC)

cv2.imshow('normal image',img)
cv2.imshow('res1',res1)
cv2.imshow('res2',res2)
while(cv2.waitKey(0) & 0xFF != ord('q')):
	pass
cv2.destroyAllWindows()