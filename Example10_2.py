# Histograms
# Hisogram of three different colors (channels)
import cv2
import numpy as np 
from matplotlib import pyplot as plt 

img = cv2.imread('photos/home.jpg')
color = ('b','g','r')
for i,col in enumerate(color):
	histr = cv2.calcHist([img],[i],None,[256],[0,255])
	plt.plot(histr,color=col)
	plt.xlim([0,255])
plt.show()