# Contours
# Find the contours of a binary image
import numpy as np 
import cv2

im = cv2.imread('photos/apple.jpg')
imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
#ret,thresh = cv2.threshold(imgray,127,255,0)
#thresh = cv2.adaptiveThreshold(imgray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
#            cv2.THRESH_BINARY,885,2)
thresh = cv2.Canny(im,100,200)
contours,hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

img_out = cv2.drawContours(im, contours,-1,(0,255,0),3)
cv2.namedWindow('img_out',cv2.WINDOW_NORMAL)
cv2.imshow('img_out' ,img_out)
while(cv2.waitKey(0) & 0xFF != ord('q')):
	pass
cv2.destroyAllWindows()
