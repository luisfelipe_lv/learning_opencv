# Edge detection
# FAST algorithm
import cv2
import numpy as np 
import matplotlib.pyplot as plt

def draw_keypoints(vis, keypoints, color = (0, 255, 255)):
    for kp in keypoints:
            x, y = kp.pt
            cv2.circle(vis, (int(x), int(y)), 2, color)

img = cv2.imread('photos/simple.jpg')

# Initiate FAST object with default values
fast = cv2.FastFeatureDetector_create()

# find and draw the keypoints
kp = fast.detect(img,None)
draw_keypoints(img, kp, color=(255,0,0))

# Print all default params
print("Threshold: ", fast.getThreshold());
print("nonmaxSuppression: ", fast.getNonmaxSuppression());
print("neighborhood: ", fast.getType());
print("Total Keypoints with nonmaxSuppression: ", len(kp));

cv2.imwrite('photos/fast_true.png',img)
cv2.namedWindow('fast_true',cv2.WINDOW_NORMAL)
cv2.namedWindow('fast_false',cv2.WINDOW_NORMAL)
cv2.imshow('fast_true',img)

# Disable nonmaxSupression
fast.setNonmaxSuppression(False);
kp = fast.detect(img,None);
print("Total Keypoints without nonmaxSuppression: ", len(kp));

draw_keypoints(img, kp, color=(255,0,0))

cv2.imwrite('photos/fast_false.png',img)
cv2.imshow('fast_false',img)
cv2.waitKey(0)
cv2.destroyAllWindows()