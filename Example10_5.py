# Histogram
# Histogram equalization
import cv2
import numpy as np 
from matplotlib import pyplot as plt

img = cv2.imread('photos/wiki.jpg',0)
equ = cv2.equalizeHist(img)
himg,bimg = np.histogram(img.flatten(),256,[0,255])
hequ,bequ = np.histogram(equ.flatten(),256,[0,255])

# Get histograms img and equ
cdfimg = himg.cumsum()
cdfequ = hequ.cumsum()

# Get normalized cumulative histograms for img and equ
cdfimg_normalized = cdfimg*himg.max() / cdfimg.max()	# normalized accumulative histogram
cdfequ_normalized = cdfequ*hequ.max() / cdfequ.max()	# normalized accumulative histogram

# Plot images
plt.subplot(221), plt.imshow(img, cmap='gray')
plt.subplot(222), plt.imshow(equ, cmap='gray')
# ---
plt.subplot(223), plt.plot(cdfimg_normalized,color='b')
plt.hist(img.flatten(),256,[0,255],color='r'), plt.xlim([0,255])
plt.legend(('cdf','histogram'),loc='upper left')
# ---
plt.subplot(224), plt.plot(cdfequ_normalized,color='b')
plt.hist(equ.flatten(),256,[0,255],color='r'), plt.xlim([0,255])
plt.legend(('cdf','histogram'),loc='upper left')

plt.show()


#res = np.hstack((img,equ))	# stacking images side-by-side
#cv2.namedWindow('res',cv2.WINDOW_NORMAL), cv2.imshow('res',res)
#while(cv2.waitKey(0) != ord('q')):
#	pass
#cv2.destroyAllWindows()