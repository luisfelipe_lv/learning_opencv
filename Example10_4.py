# Histograms
# Introduction to histogram equalization
import cv2
import numpy as np 
from matplotlib import pyplot as plt 

img = cv2.imread('photos/wiki.jpg',0)

hist,bins = np.histogram(img.flatten(),256,[0,255])

cdf = hist.cumsum()
cdf_normalized = cdf*hist.max() / cdf.max()

plt.subplot(121)
plt.imshow(img,cmap='gray')
plt.subplot(122)
plt.plot(cdf_normalized, color='b')
plt.hist(img.flatten(),256,[0,255],color='r')
plt.xlim([0,255])
plt.legend(('cdf','histogram'),loc='upper left')
plt.show()