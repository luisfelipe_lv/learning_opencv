# Contour Features
# Contour Convexity
import cv2
import numpy as np 

img = cv2.imread('photos/hand.jpg',1)
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(gray,240,255,1)
contours,hierarcy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

for i in range(len(contours)):
	convex = cv2.isContourConvex(contours[i])
	hull = cv2.convexHull(contours[i])
	print("Contour[",i,"] convex?", convex)
	print("hull[",i,"] convex?",cv2.isContourConvex(hull))
	cnt_show = cv2.drawContours(img.copy(),[contours[i]],0,(0,255,0),3)
	cv2.namedWindow('cnt['+str(i)+"]", cv2.WINDOW_NORMAL)
	cv2.imshow('cnt['+str(i)+"]",cnt_show)

cv2.waitKey(0) & 0xFF
cv2.destroyAllWindows()