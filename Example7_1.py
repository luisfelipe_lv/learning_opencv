# 2D convolution (Image Filtering)
# Averagin filter
import cv2
import numpy as np 
from matplotlib import pyplot as plt 

img = cv2.imread('photos/lund-oldtown.jpg',1)

kernel = np.ones((5,5),np.float32)/25
dst = cv2.filter2D(img,-1,kernel)

cv2.imshow('Original',img)
cv2.imshow('Filtered',dst)
while(cv2.waitKey(0) != ord('q')):
	pass
cv2.destroyAllWindows()