# Contour Features
# Convex Hull
import cv2
import numpy as np 

img = cv2.imread('photos/hand.jpg',1)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(gray,230,255,1)
contours,hierarchy = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

# Looks for the maximum amount of points as a contour for hulling
index = 0
for i in range(len(contours)):
	if contours[i].shape > contours[index].shape:
		index = i

cnt = contours[index]
hull = cv2.convexHull(cnt)
hull_show = cv2.drawContours(img,[hull],0,(0,255,0),3)

cv2.namedWindow('hull',cv2.WINDOW_NORMAL)
cv2.imshow('hull',hull_show)
cv2.waitKey(0) & 0xFF
cv2.destroyAllWindows()