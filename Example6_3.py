# Geometric Transformations of Images
# Rotation
import cv2
import numpy as np 

img = cv2.imread('photos/messi.jpg',0)
rows,cols = img.shape

M = cv2.getRotationMatrix2D((cols/2,rows/2),90,1)
dst = cv2.warpAffine(img,M,(cols,rows))

cv2.imshow('Original',img)
cv2.imshow('Rotation',dst)
cv2.waitKey(0) & 0xFF
cv2.destroyAllWindows()