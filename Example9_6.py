# Contour Features
# Bounding Rectangles
import cv2
import numpy as np 

img = cv2.imread('photos/thunder.png',1)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(gray,240,255,0)
contours,hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
cnt = contours[0]

x,y,w,h = cv2.boundingRect(cnt)
rect = cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)

# Now we add a tilted rectangle
min_rect = cv2.minAreaRect(cnt)
box = cv2.boxPoints(min_rect)
box = np.int0(box)
out = cv2.drawContours(rect.copy(),[box],0,(0,0,255),2)

cv2.namedWindow('rect',cv2.WINDOW_NORMAL), cv2.namedWindow('full out',cv2.WINDOW_NORMAL)
cv2.imshow('rect',rect), cv2.imshow('full out', out)
cv2.waitKey(0) & 0xFF
cv2.destroyAllWindows()
