# Corner Detection
# SIFT
import cv2
import numpy as np 

img = cv2.imread('photos/home.jpg')
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

sift = cv2.SIFT()
kp = sift.detect(gray, None)

img = cv2.drawKeypoints(gray,kp)

cv2.namedWindow('sift_keypoints',cv2.WINDOW_NORMAL)
cv2.imshow('sift_keypoints',img)
cv2.waitKey(0) & 0xFF
cv2.destroyAllWindows()