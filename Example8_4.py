# Image pyramids
import cv2
import numpy as np 

img = cv2.imread('photos/messi.jpg',1)
lower_reso = cv2.pyrDown(img)
higher_reso2 = cv2.pyrUp(lower_reso)

cv2.imshow('img',img),cv2.imshow('lower_reso',lower_reso),cv2.imshow('higher_reso2',higher_reso2)
while(cv2.waitKey(0)!=ord('q')):
	pass
cv2.destroyAllWindows()
