import numpy as np
import cv2
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="path to input image")
args = vars(ap.parse_args())

img = cv2.imread(args['image'])
cimg = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)

# create a CLAHE object (Arguments are optional).
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
cimg[:,:,0] = clahe.apply(cimg[:,:,0])
img2 = cv2.cvtColor(cimg, cv2.COLOR_YCrCb2BGR)

cv2.namedWindow('original',cv2.WINDOW_NORMAL),cv2.namedWindow('equalized',cv2.WINDOW_NORMAL)
cv2.imshow('original',img)
cv2.imshow('equalized',img2)
cv2.waitKey(0) & 0xFF
cv2.destroyAllWindows()