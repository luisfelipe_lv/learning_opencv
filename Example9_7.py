# Contour Features
# Minimum Enclosing Circle
import cv2
import numpy as np 

img = cv2.imread('photos/thunder.png',1)
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
_,thresh = cv2.threshold(gray,240,255,0)
contours,_ = cv2.findContours(thresh,cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
cnt = contours[0]

# Fitting minimum enclosing circle
(x,y),radius = cv2.minEnclosingCircle(cnt)
center = (int(x), int(y))
radius = int(radius)
circle = cv2.circle(img.copy(),center,radius,(0,255,0),2)

# Fitting an elipse
ellipse2 = cv2.fitEllipse(cnt)
ellipse = cv2.ellipse(img,ellipse2,(0,255,0),2)

cv2.namedWindow('circle',cv2.WINDOW_NORMAL),cv2.namedWindow('ellipse',cv2.WINDOW_NORMAL)
cv2.imshow('circle',circle), cv2.imshow('ellipse',ellipse)
cv2.waitKey(0) & 0xFF
cv2.destroyAllWindows()