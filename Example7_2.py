# 2D convolution (Image Filtering)
# Averagin filter (blur filter)
import cv2
import numpy as np 
from matplotlib import pyplot as plt 

img = cv2.imread('photos/lund-oldtown.jpg')

blur_box = cv2.blur(img,(5,5))
blur_gaussian = cv2.GaussianBlur(img,(5,5),0)
median = cv2.medianBlur(img,5)
bilateral = cv2.bilateralFilter(img,9,75,75)

cv2.imshow('Original',img), cv2.imshow('Blured Box',blur_box), cv2.imshow('median',median)
cv2.imshow('Gaussian Blured', blur_gaussian), cv2.imshow('bilateral',bilateral)
while(cv2.waitKey(0)!=ord('q')):
	pass
cv2.destroyAllWindows()
